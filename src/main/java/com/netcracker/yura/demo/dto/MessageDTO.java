package com.netcracker.yura.demo.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MessageDTO {

    private static final int ALL_RECEIVERS = 0;

    private String text;
    private int sender;
    private int receiver;
    private String time;

}
