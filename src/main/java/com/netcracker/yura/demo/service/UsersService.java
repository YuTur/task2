package com.netcracker.yura.demo.service;

import com.netcracker.yura.demo.dto.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UsersService {

    private List<UserDTO> users;


    public UsersService(){
        users = new ArrayList<UserDTO>() {
        };
        users.add(new UserDTO("ALL","login","password", ""));
    }
    
    private boolean checkLogin(String login){
        for (UserDTO user: users
             ) {if (user.getLogin().equals(login)){
                 return true;
        }
            
        }
        return false;
    }

    private UserDTO getUser(UserDTO enterUser){
        for (UserDTO user: users
             ) {if (user.getLogin().equals(enterUser.getLogin())) {
            return user;
        }

        }
        return null;
    }

    public String getUserName(int id){
        return users.get(id).getName();
    }

    public int getUserIdByUUID(String uuid){
        for (UserDTO user: users
        ) {
            if (user.getUuid().equals(uuid)) {
                return users.indexOf(user);
            }
        }
        return 0;
    }

    public int getUserIdByLogin (String login){
        for (UserDTO user: users
        ) {
            if (user.getLogin().equals(login)) {
                return users.indexOf(user);
            }
        }
        return 0;
    }

    public ResponseEntity register(UserDTO user){
        if (checkLogin(user.getLogin())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This login is already used, please try another one");
        };
        user.setUuid(UUID.randomUUID().toString());
        users.add(user);
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }

    public ResponseEntity enter(UserDTO enterUser){
        UserDTO user = getUser(enterUser);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This login doesn't exist");
        };
        if (!user.getPassword().equals(enterUser.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Wrong password");
        }

        //users.add(user);
        user.setUuid(UUID.randomUUID().toString());
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }


}
