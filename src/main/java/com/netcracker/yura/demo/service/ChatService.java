package com.netcracker.yura.demo.service;

import com.netcracker.yura.demo.dto.SendingMessageDTO;
import com.netcracker.yura.demo.dto.MessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ChatService {

    private Map<Integer, List<MessageDTO>> chats;
    @Autowired
    private UsersService usersService;

    public ChatService(){
        chats = new HashMap< Integer, List<MessageDTO>>();
    }

    public ResponseEntity<String> getMessages(String uuid){
        if (uuid.equals("")){
            return ResponseEntity.status(HttpStatus.OK).body(getMessages(0));
        }
        return ResponseEntity.status(HttpStatus.OK).body(getMessages(usersService.getUserIdByUUID(uuid)));
    }

    public ResponseEntity addMessage(SendingMessageDTO message){
        addMessage(message.getText(),usersService.getUserIdByUUID(message.getUuid()),usersService.getUserIdByLogin(message.getReceiverLogin()));
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }

    public void addMessage(String text, int senderId, int receiverId){
        List<MessageDTO> chat;
        if (chats.containsKey(receiverId)){
            chat = chats.get(receiverId);
        } else {
            chat = new ArrayList<MessageDTO>();
            chats.put(receiverId,chat);
        }

        chat.add(new MessageDTO(text,senderId, receiverId,new SimpleDateFormat("hh:mm").format(new Date())));
    }

    public String getMessages(int receiverId){
        List<MessageDTO> chat;
        if (chats.containsKey(receiverId)){
            chat = chats.get(receiverId);
            StringBuilder result = new StringBuilder();
            for (MessageDTO x : chat){
                result.append(x.getTime())
                        .append(" ")
                        .append(usersService.getUserName(x.getSender()))
                        .append(" >> ")
                        .append(x.getText())
                        .append("\n");
            }
            return result.toString();
        }
        else {
            chat = new ArrayList<MessageDTO>();
            chats.put(receiverId,chat);
            return "No messages";
        }


    }





}
