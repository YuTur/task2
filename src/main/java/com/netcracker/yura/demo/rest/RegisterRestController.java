package com.netcracker.yura.demo.rest;

import com.netcracker.yura.demo.dto.UserDTO;
import com.netcracker.yura.demo.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/register")
@RestController
public class RegisterRestController {

    //private Logger logger = LoggerFactory.getLogger(RegisterRestController.class);

    @Autowired
    private UsersService registerService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody UserDTO userDTO) {

        return registerService.register(userDTO);
    }


}
