package com.netcracker.yura.demo.rest;

import com.netcracker.yura.demo.dto.UserDTO;
import com.netcracker.yura.demo.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/enter")
@RestController
public class EnterRestController {

    @Autowired
    private UsersService enterService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity enter(@RequestBody UserDTO userDTO) {

        return enterService.enter(userDTO);
    }


}