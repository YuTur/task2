package com.netcracker.yura.demo.rest;

import com.netcracker.yura.demo.dto.SendingMessageDTO;
import com.netcracker.yura.demo.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RequestMapping("/get")
@RestController
public class GetChatRestController {




        //private Logger logger = LoggerFactory.getLogger(RegisterRestController.class);

        @Autowired
        private ChatService chatService;

        @RequestMapping(method = RequestMethod.POST)
        public ResponseEntity<String> get(@RequestBody String uuid) {

            return chatService.getMessages(uuid);

        }


}
