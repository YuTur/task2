package com.netcracker.yura.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SendingMessageDTO {
    private String uuid;
    private String receiverLogin;
    private String text;
}
