package com.netcracker.yura.demo.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class UserDTO {

    private String login;
    private String password;
    private String uuid;
    private String name;

    public String getName() {
        if (name == null){
            return login;
        }
        return name;
    }

    public UserDTO(String login, String password, String uuid, String name) {
        this.login = login;
        this.password = password;
        this.uuid = uuid;
        /*if (name == null) {
            this.name = login;
        } else {*/
            this.name = name;
        //}
    }


}
